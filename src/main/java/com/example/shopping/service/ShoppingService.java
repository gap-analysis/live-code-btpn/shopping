package com.example.shopping.service;

import com.example.shopping.dto.ShoppingDTO;
import com.example.shopping.dto.ShoppingRecomendDTO;
import com.example.shopping.entity.Shopping;
import com.example.shopping.repository.ShoppingRepository;
import com.example.shopping.repository.ShoppingRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class ShoppingService {

    @Autowired
    ShoppingRepository shoppingRepository;

    public ResponseEntity<?> addItem(ShoppingDTO request){
        Shopping shopping = new Shopping();
        shopping.setItemType(request.getItemType());
        shopping.setItemName(request.getItemName());
        shopping.setPrice(request.getPrice());
        shoppingRepository.save(shopping);

        return ResponseEntity.ok(shopping);
    }

    public ResponseEntity<?> getAllItem (){

        List item = shoppingRepository.findAll();

        return ResponseEntity.ok(item);
    }

    public ResponseEntity<?> recomendItem(ShoppingRecomendDTO requset){
        List<Shopping> listKeyboard = shoppingRepository.findByItemType("Keyboard");
        List<Shopping> listMouse = shoppingRepository.findByItemType("Mouse");

        int maxSpend = 0;
        Shopping bestKeyboard = null;
        Shopping bestMouse = null;

        for (Shopping keyboard : listKeyboard) {
            for (Shopping mouse : listMouse) {
                log.info("__________");
                log.info(String.valueOf(keyboard.getPrice()));
                log.info(String.valueOf(mouse.getPrice()));
                int total = keyboard.getPrice() + mouse.getPrice();
                if (total <= requset.getBudget() && total > maxSpend) {
                    maxSpend = total;
                    bestKeyboard = keyboard;
                    bestMouse = mouse;
                }
            }
        }

        String message;
        if (bestKeyboard != null && bestMouse != null) {
            message = "Best combination: " + bestKeyboard.getItemType() + " at " + bestKeyboard.getPrice() + " and " + bestMouse.getItemType() + " at " + bestMouse.getPrice() + ". Total: " + maxSpend;

            return ResponseEntity.ok(message);
        } else {
            message = "No valid combination found within budget.";
            return ResponseEntity.ok(message);
        }
    }
}
