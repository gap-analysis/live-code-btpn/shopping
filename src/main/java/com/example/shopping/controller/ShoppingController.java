package com.example.shopping.controller;

import com.example.shopping.dto.ShoppingDTO;
import com.example.shopping.dto.ShoppingRecomendDTO;
import com.example.shopping.service.ShoppingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping
public class ShoppingController {

    @Autowired
    ShoppingService shoppingService;

    @GetMapping("/getAll")
    @ResponseBody
    public ResponseEntity<?> getAllItem (){
        return shoppingService.getAllItem();
    }

    @PostMapping("/add")
    @ResponseBody
    public ResponseEntity<?> addItem (@RequestBody ShoppingDTO request){
        return shoppingService.addItem(request);
    }

    @GetMapping("/recomendation")
    @ResponseBody
    public ResponseEntity<?> recommendItem (@RequestBody ShoppingRecomendDTO request){
        return shoppingService.recomendItem(request);
    }
}
