package com.example.shopping.repository;

import com.example.shopping.entity.Shopping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShoppingRepository extends JpaRepository<Shopping, Long> {
    List <Shopping> findByItemType(String type);
}
