package com.example.shopping.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "shopping_item")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Shopping {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "item_type")
    private String itemType;

    @Column(name = "item_name")
    private String itemName;

    @Column(name = "price")
    private int price;
}
